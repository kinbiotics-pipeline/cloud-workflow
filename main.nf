#!/usr/bin/env nextflow

nextflow.enable.dsl=2

include {get_guppy_config;guppy} from './lib/basecalling.nf'
include {flye;medaka} from './lib/assembly.nf'
include {rgi;gtdbtk} from './lib/analysis.nf'
include {publish_rgi;publish_gtdbtk} from './lib/publish.nf'

workflow {

    fast5 = channel.fromPath(params.fast5)

    /* Get the run ID from parent folder name */
    runid = channel.of(file(params.fast5).collect()*.toString()[-2])

    /* Tuple out of ID and FAST5 file as input for the analysis */
    input_tuple = runid.combine(fast5)

    /* Basecalling with high accuracy
        First the sequencing configuration is extracted from the MinKNOW summary file,
        then the actual basecalling is started. */
    final_sum = file(params.final_summary)
    get_guppy_config(final_sum)

    reads = guppy(input_tuple, get_guppy_config.out.guppy_cfg)

    /* Assembly and polishing */
    assembly = flye(reads)

    consensus = medaka(reads,assembly)

    /* ABR detection with RGI */
    abr = rgi(consensus)

    /* Species classification with GTDB-tk */
    gtdb = file(params.gtdb)
    gtdbtk_res = gtdbtk(consensus, gtdb)

    /* Publish results */
    publish_rgi(abr)
    publish_gtdbtk(gtdbtk_res)
}


#!/usr/bin/env nextflow

nextflow.enable.dsl=2

/*
 * Assemble reads with flye
 */
process flye {

    container 'quay.io/biocontainers/flye:2.9.2--py310h2b6aa90_2'

    input:
        tuple val(runid), path(reads)

    output:
        tuple val(runid), path("assembly.fasta")

	publishDir "${params.publishdir}", saveAs: {filename -> "${runid}_assembly.fasta"}


    """
	flye --nano-raw ${reads} --out-dir . --threads 12
    """
}

/*
 * Polish assembly with medaka
 */
process medaka {

    label "gpu"
    container 'quay.io/biocontainers/medaka:1.8.0--py310hb7fe8e6_0'

    input:
        tuple val(runid), path(reads)
        tuple val(runid), path(assembly)

    output:
        tuple val(runid), path("consensus.fasta")

	publishDir "${params.publishdir}", saveAs: {filename -> "${runid}_consensus.fasta"}


    """
    cat ${reads} > allreads.fastq.gz
    medaka_consensus -i allreads.fastq.gz -d ${assembly} -t 12 -o .
    """

}


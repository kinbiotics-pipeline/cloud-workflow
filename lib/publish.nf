#!/usr/bin/env nextflow

nextflow.enable.dsl=2

/*
 * Publish RGI results
 */
process publish_rgi {

        container 'quay.io/dlehmenk/csvkit-curl-jq:latest'
        
        input:
            tuple val(runid), path(abrjson)


        """
        # remove fields containing sequences, beause this would take up much space in the database and is not needed. Also flatten the JSON structure
    	cat ${abrjson} |  jq '.[] |= map_values(del(.ARO_category, .query, .match, .sequence_from_db, .sequence_from_broadstreet, .dna_sequence_from_broadstreet, .orf_dna_sequence, .orf_prot_sequence, .snp, .query_snp)) | .[][] += { "source": "${params.machine_id}" }' |  jq '[ to_entries[] | .value[] += {"hit_seq_id": .key} | .value | to_entries[] | .value += {"hit_id": .key} | .value ]' | curl --header "Content-Type: application/json" --request POST --data @- http://${params.db_api_server}/${runid}/${params.db_api_rgi_endpoint}
        """
}

/*
 * Publish GTDB-tk results
 */
process publish_gtdbtk {

        container 'quay.io/dlehmenk/csvkit-curl-jq:latest'
        
        input:
            tuple val(runid), path(gtdbtk_summary)


        """
        # convert csv to json, add source field, rename other_related... column and upload to api
    	csvjson --tabs --no-inference "${gtdbtk_summary}" | jq '.[] += { "source": "${params.machine_id}"}' | jq '[ .[] | .["other_related_references"] = ."other_related_references(genome_id,species_name,radius,ANI,AF)" | del(.["other_related_references(genome_id,species_name,radius,ANI,AF)"]) ]' | curl --header "Content-Type: application/json" --request POST --data @- http://${params.db_api_server}/${runid}/${params.db_api_gtdb_endpoint}
        """
}



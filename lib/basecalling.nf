#!/usr/bin/env nextflow

nextflow.enable.dsl=2

/*
 * Get kit and flowcell data from final_summary.tsv
 */
process get_guppy_config {

    input:
        path final_summary

    output:
        tuple env(flowcell), env(kit), emit: guppy_cfg

    shell:
    '''
    proto=`grep '^protocol=' !{final_summary} | cut -d ':' -f 2-3`
    flowcell="${proto%%:*}"
    kit="${proto##*:}"
    '''
}


/*
 * Basecall reads with guppy
 */
process guppy {

    label "gpu"
    container 'genomicpariscentre/guppy-gpu'

    input:
        tuple val(runid), path(fast5)
        tuple val(flowcell), val(kit)

    output:
        tuple val(runid), path("pass/*.fastq.gz")

	publishDir "${params.publishdir}", saveAs: {filename -> "${runid}_${filename}"}


    """
    guppy_basecaller -x auto -i "${fast5}" --compress_fastq -s . --flowcell "${flowcell}" --kit "${kit}"
    """
}

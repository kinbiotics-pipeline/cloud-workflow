#!/usr/bin/env nextflow

nextflow.enable.dsl=2

/*
 * ABR detection with rgi
 */
process rgi {

        container 'quay.io/biocontainers/rgi:6.0.2--pyha8f3691_0'

        input:
            tuple val(runid), path(assembly)

        output:
            tuple val(runid), path("abr.json")

        publishDir "${params.publishdir}", saveAs: {filename -> "${runid}_${filename}"}


        """
    	rgi main --input_sequence ${assembly} --output_file abr --input_type contig --clean
        """
}

/*
 * Species classification detection with GTDB-tk
 */
process gtdbtk {

        container 'quay.io/biocontainers/gtdbtk:2.3.0--pyhdfd78af_2'

        // Hand the GTDB data into the container
        containerOptions "--volume ${task.workDir}/${gtdbtk_refdata}:/opt/refdata/"

        input:
            tuple val(runid), path(assembly, stageAs: "genome_dir/*")
            path(gtdbtk_refdata)

        output:
            tuple val(runid), path("gtdb-tk/gtdbtk.bac120.summary.tsv")

        publishDir "${params.publishdir}", saveAs: {filename -> "${runid}_${filename}"}

        """
        export GTDBTK_DATA_PATH="/opt/refdata"
        gtdbtk classify_wf --genome_dir genome_dir --extension fasta --out_dir ./gtdb-tk --cpus 12 --skip_ani_screen
        """
}



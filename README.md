KINBIOTICS Cloud Workflow
=========================

The Workflow is started automatically through the [KINBIOTICS Cloud API](https://gitlab.ub.uni-bielefeld.de/kinbiotics-pipeline/cloud-workflow-api).

To setup, the [GTDB-Tk reference data](https://ecogenomics.github.io/GTDBTk/installing/index.html#gtdb-tk-reference-data) needs to be downloaded and unpacked and within the Cloud API all directories need to be set correcly in the config files.  Also [nextflow](https://www.nextflow.io/) and [Docker](https://docker.io) have to be installed, as well as apropiate GPU drivers (e.g. for [nvidia](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html)). The worklfow has been tested with Nextflow 22.10.3 (with OpenJDK 17.0.7) and  Docker 20.10.21.
